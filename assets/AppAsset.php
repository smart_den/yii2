<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    // Подключение стилей проекта
    public $css = [
        'css/site.css',
        'css/starrr.css',
        'css/fotorama.css',
        'css/styles.css',
    ];
    // Подключение скриптов проекта
    public $js = [
        'js/starrr.js', 
        'js/main.js',      
        'js/fotorama.js',
        'js/lumino.glyphs.js',    
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
