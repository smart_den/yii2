<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Users;
use app\controllers\MainController;
use app\models\PeculiaritiesNature;
use app\models\RestNature;
use app\models\Forest;
use app\models\Beach;
use app\models\Likes;
use app\models\Comments;
use yii\helpers\Json;


class AjaxController extends MainController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    
    public function actionAddfoto()
    {   
        $file = $_FILES['image'];
        if ($file && $file['error'] == 0) {

            $tmp = $file['tmp_name'];
            $path = 'images/';
            $name = $file['name'];
            $expansion = explode('/', $file['type'])[1];

            if ($expansion == 'jpeg' || $expansion == 'jpg' || $expansion == 'png') {
                $fullPath = $path.md5(time()).$name;
                if (move_uploaded_file($tmp, $fullPath)) {
                    return Json::encode('/'.$fullPath);
                }
            }
        }   
    }

    public function actionLikeobject()
    {
        /*Добавляет 1 к лайкам обьекта*/
        $id = $_POST['id_object'];
        $model = RestNature::findOne($id);
        $model->object_likes += 1;
        $model->save();

        /*Делаем запись в таблицу лайков*/
        $like = new Likes();
        $like->id_user = Yii::$app->user->id;
        $like->type_object = 'object';
        $like->type_like = 1;
        $like->id_object = $id;
        $like->save();
    }

    public function actionDellikeobject()
    {
        /*Добавляет -1 к лайкам обьекта*/
        $id = $_POST['id_object'];
        $model = RestNature::findOne($id);
        $model->object_likes -= 1;
        $model->save();

        /*Удаляем запись в таблице лайков*/
        $like = Likes::find()->where(['id_user'=> Yii::$app->user->id,'type_object'=>'object','id_object'=>$id,'type_like'=>1])->one();
        $like->delete();
    }

    public function actionDislikeobject()
    {
        /*Добавляет 1 к Dis лайкам обьекта*/
        $id = $_POST['id_object'];
        $model = RestNature::findOne($id);
        $model->object_dislikes += 1;
        $model->save();

        /*Делаем запись в таблицу лайков*/
        $like = new Likes();
        $like->id_user = Yii::$app->user->id;
        $like->type_object = 'object';
        $like->type_like = 0;
        $like->id_object = $id;
        $like->save();
    }

    public function actionDeldislikeobject()
    {
        /*Добавляет -1 к Dis лайкам обьекта*/
        $id = $_POST['id_object'];
        $model = RestNature::findOne($id);
        $model->object_dislikes -= 1;
        $model->save();

        /*Удаляем запись в таблице лайков*/
        $like = Likes::find()->where(['id_user'=> Yii::$app->user->id,'type_object'=>'object','id_object'=>$id,'type_like'=>0])->one();
        $like->delete();
    }

     public function actionAddcomment(){
        // var_dump($_POST);
        // var_dump(Yii::$app->user->isGuest);
        // var_dump(Yii::$app->user->id);
        // var_dump(Yii::$app->user->identity->name);
        // var_dump(Yii::$app->request->post()['Comments']);

        if(Yii::$app->request->isPost){
            $comment = new Comments();
            $comment->object_id = Yii::$app->request->post()['Comments']['object_id'];
            $comment->parent_id = Yii::$app->request->post()['Comments']['parent_id'];

            if(Yii::$app->user->identity->name){
                $comment->author_name =  Yii::$app->user->identity->name;
                $comment->author_id =  Yii::$app->user->id;
            }else{
               $comment->author_name =  Yii::$app->request->post()['Comments']['author_name']; 
               $comment->author_id = 0; 
            }
                            
            $comment->message =  Yii::$app->request->post()['Comments']['message'];
            $comment->ip =  Yii::$app->request->userIP;  

            // var_dump($comment->save()) ;                  

            if ($comment->save()) {
                return 'true';
            }else {
                return 'bad';
            }
        } // end_if (Yii::$app->request->isPost)
    } //end_actionAddcomment()

}
