<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Users;
use app\controllers\MainController;
use app\models\Peculiarities;
use app\models\ObjectFoto;
use app\models\RestNature;
use app\models\Forest;
use app\models\Beach;
use app\models\Likes;
use app\models\Comments;
use yii\helpers\Json;
use yii\data\Pagination;  // для пагинации


class ObjectController extends MainController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionList()
    { 

        return $this->render('index');
    }


    public function actionAddobject()
    {   
        $post = Yii::$app->request->post();
        $RestNature = new RestNature();
        $PeculiaritiesNature = new Peculiarities();
        $Forest = Forest::find()->asArray()->all();
        $Beach = Beach::find()->asArray()->all();

        $ArrForest['0'] = 'Выберите лес';
        foreach ($Forest as $key => $value) {
            $ArrForest[$value['idForest']] = $value['typeTree'];
        }

        $ArrBeach['0'] = 'Выберите пляж';
        foreach ($Beach as $keyBeach => $valueBeach) {
            $ArrBeach[$valueBeach['idBeach']] = $valueBeach['typeBeach'];
        }
        if ($RestNature->load($post) && $PeculiaritiesNature->load($post)) {
            if ($RestNature->save()) {
                $id = Yii::$app->db->getLastInsertID();
                $PeculiaritiesNature->idObject = $id;
                if ($PeculiaritiesNature->save()) {
                     $this->redirect('/object/editobject?id='.$id,302);   
                }
            }
         }
        return $this->render('addobject',['RestNature'=>$RestNature,'PeculiaritiesNature'=>$PeculiaritiesNature,'ArrForest'=>$ArrForest,'ArrBeach'=>$ArrBeach]);
    }

    public function actionEditobject()
    {   
    	$id = $post = Yii::$app->request->get('id');
    	$post = Yii::$app->request->post();
        $RestNature = RestNature::findOne($id);
        $Peculiarities = Peculiarities::find()->where(['idObject'=>$id])->one();
        $Forest = Forest::find()->asArray()->all();
        $Beach = Beach::find()->asArray()->all();

        $ArrForest['0'] = 'Выберите лес';
        foreach ($Forest as $key => $value) {
            $ArrForest[$value['idForest']] = $value['typeTree'];
        }

        $ArrBeach['0'] = 'Выберите пляж';
        foreach ($Beach as $keyBeach => $valueBeach) {
            $ArrBeach[$valueBeach['idBeach']] = $valueBeach['typeBeach'];
        }

        if ($post) {
	        if ($RestNature->load($post) && $Peculiarities->load($post)) {
	            if ($RestNature->save()) {
	                $Peculiarities->idObject = $id;
	                if ($Peculiarities->save()) {
	  
	                }
	            }
	         }
     	}




        return $this->render('editobject',['RestNature'=>$RestNature,'Peculiarities'=>$Peculiarities,'ArrForest'=>$ArrForest,'ArrBeach'=>$ArrBeach]);
    }

    public function actionShowobjects()
    {
        $this->view->title = 'Объекты';

        $objects = RestNature::find()->leftJoin('peculiarities' ,'`restNature`.`id` = `peculiarities`.`idObject`')->with('peculiarities');

        $pagination = new Pagination([
           'defaultPageSize' => 9,                 // на странице
           'totalCount' => $objects->count()       // всего
        ]);

        $objects = $objects->offset($pagination->offset)->limit($pagination->limit)->all();

        foreach($objects as $object){
            $object->description =  $object->getShortText($object->description);
        }

        $Forest = Forest::find()->all();
        $Beach = Beach::find()->all();

        return $this->render('objects', compact('objects', 'Forest', 'Beach', 'comments', 'pagination'));
    }

    public function actionShowobject()
    {
        $this->view->title = 'Объект';
        $object = RestNature::find()->where(['id'=> Yii::$app->request->get()['id']])->one();    

        /*----Count-Views----*/
        if (Yii::$app->session->has('view_object')) {
            if (!Yii::$app->session->get('view_object')[$object->id]) {
                $objectView = RestNature::find()->where(['id'=> Yii::$app->request->get()['id']])->one();
                $objectView->views += 1;
                $objectView->save();
                $views = Yii::$app->session->get('view_object');
                $views[$object->id] = 1;
                Yii::$app->session->set('view_object',$views);
            }
        }else{
            $objectView = RestNature::find()->where(['id'=> Yii::$app->request->get()['id']])->one();
            $objectView->views += 1;
            $objectView->save();
            $arr[$object->id] = 1;
            Yii::$app->session->set('view_object',$arr);
        }

        $object_photo = ObjectFoto::find()->where(['object_id'=> Yii::$app->request->get()['id']])->all();
        $peculiarities = Peculiarities::find()->where(['idObject'=> Yii::$app->request->get()['id']])->one();        
        $forest = Forest::find()->where(['idForest'=>$peculiarities->forest])->one();
        $beach = Beach::find()->where(['idBeach'=>$peculiarities->beach])->one();


        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->id;
            $user = Likes::find()->where(['id_user'=>$id,'type_object'=>'object','id_object'=>$object->id])->all();
            foreach ($user as $key => $value) {
                if ($value->type_like == 0) {
                    $user['dislike'] = 1;
                }else{
                    $user['like'] = 1;
                }
            }
        }

       


        $comments = Comments::find()->asArray()->where(['object_id' => $object->id, 'status'=> '1'])->all();
        $count = count($comments);
        // var_dump($comments);
        $tree = [];
        $arr_links = [];

  foreach ($comments as $key => $value) {
            if($value['parent_id'] == 0){
                $tree[$value['id']] = $value;
                $arr_links[$value['id']] = &$tree[$value['id']];
            }
            else{
                $arr_links[$value['parent_id']]['childrens'][$value['id']] =  $value;
                $arr_links[$value['id']] = &$arr_links[$value['parent_id']]['childrens'][$value['id']];
            }
        }

        $model = new Comments;      

        return $this->render('object', compact('object', 'forest', 'beach', 'object_photo', 'peculiarities', 'comments', 'user', 'count', 'model','tree'));


    }

}


