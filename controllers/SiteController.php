<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Users;
use app\controllers\MainController;
use app\models\PeculiaritiesNature;
use app\models\RestNature;
use app\models\Articles;
use app\models\Photoblog;
use app\models\Comments;
use app\models\Forest;
use app\models\Beach;
use yii\helpers\Json;
use yii\web\UploadedFile;   //для загрузки фоток
use yii\helpers\Url;
use yii\data\Pagination;  // для пагинации


class SiteController extends MainController
{
    //  если что-то сохранилось вызвать метод бехаворс  без if и тд
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    { 
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {

            //бросить на ошибку если не залогине
            throw new \yii\base\Exception(404);
        }

        $registerform = new SignupForm();

        if ($registerform->load(Yii::$app->request->post())) {
            $lastId = $registerform->SingUp();
            if ($lastId) {
                if ($registerform->Login($lastId)) {
                   return $this->goHome();
                }
            }else{
                $session = Yii::$app->session; // запуск сессии
                $session->setFlash('EmailExists', 'Пользователь с таким Email уже существует'); //страничная сессия
            }
        }

        return $this->render('signup',['registerform'=>$registerform]);
    }


    public function actionArticles()
    {
        $articles = Articles::find();

        $pagination = new Pagination([
           'defaultPageSize' => 4,                 // на странице
           'totalCount' => 12       // всего
        ]);

        $articles = $articles->offset($pagination->offset)->limit($pagination->limit)->all();
        // var_dump($pagination);
        // die();


        return $this->render('articles', compact('articles', 'pagination'));  //http://basic.com/articles
    }

    public function actionArticle()
    {

        $article = Articles::find()-> where(['id'=> Yii::$app->request->get()['id']])->one();  //http://basic.com/article?id=2
        $article->hits += 1 ; // кол-во просмотров статьи
        $article->save(); // сохранение

        if(Yii::$app->request->isPost){
            $comment = new Comments();
            $comment->parent_id_comment =  Yii::$app->request->post()['Comments']['parent_id_comment'];
            $comment->comment_author =  Yii::$app->request->post()['Comments']['comment_author'];
            $comment->data_create =  date("Y-m-d H:i:s");
            $comment->message =  Yii::$app->request->post()['Comments']['message'];
            $comment->save();
            //работает но ее перебивает слудующая строка
            Yii::$app->session->setFlash('success', 'Данные приняты');
            //перезагрузим страницу после пост-запроса обязательно чтобынебыло повторной отправки формы
            Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl());


            ( $_FILES['Comments']['name']['img'] ? $comment->img = UploadedFile::getInstance($comment, 'img') : "");


            if( $comment->img && $comment->img->error == 0){
                $comment->img->saveAs(Yii::getAlias('images/' . $comment->img->baseName. md5(microtime().uniqid().rand(0,9999)).'.'. $comment->img->extension));
                $comment->img = $comment->img->baseName.md5(microtime().uniqid().rand(0,9999)).'.'.$comment->img->extension;
                $comment->save();
                Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl());
            }
        }

        $comments = Comments::find()->where(['parent_id_comment' => $article->id])->all();

        return $this->render('article', compact('article', 'comments'));
    }




}
