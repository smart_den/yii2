-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 29 2017 г., 22:04
-- Версия сервера: 5.6.34
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `tree`
--

-- --------------------------------------------------------

--
-- Структура таблицы `restNature`
--

CREATE TABLE `restNature` (
  `id` int(10) UNSIGNED NOT NULL,
  `longitude` int(11) NOT NULL,
  `latitude` int(11) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `purityEvaluation` int(11) NOT NULL,
  `purityDescription` varchar(1000) NOT NULL,
  `beautyEvaluation` int(11) NOT NULL,
  `beautyDescription` varchar(1000) NOT NULL,
  `food` tinyint(1) NOT NULL,
  `cost` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `restNature`
--

INSERT INTO `restNature` (`id`, `longitude`, `latitude`, `description`, `purityEvaluation`, `purityDescription`, `beautyEvaluation`, `beautyDescription`, `food`, `cost`, `image`) VALUES
(1, 123, 123, 'asd', 1111111111, '22222222222', 2147483647, '4444444444444', 127, 2147483647, ''),
(2, 123, 123, 'sd', 6, 'ad', 6, 'as', 0, 0, 'images/e9ae4c063963dc84f16beebc1b944355aaaaaa.jpg'),
(3, 123, 123, 'sd', 6, 'ad', 6, 'as', 0, 0, 'images/e9ae4c063963dc84f16beebc1b944355aaaaaa.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `restNature`
--
ALTER TABLE `restNature`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `restNature`
--
ALTER TABLE `restNature`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
