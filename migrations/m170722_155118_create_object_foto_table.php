<?php

use yii\db\Migration;

/**
 * Handles the creation of table `object_foto`.
 */
class m170722_155118_create_object_foto_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('object_foto', [
            'id' => $this->primaryKey()->unsigned(),
            'object_id' => $this->integer(),
            'name_img' => $this->string(255),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('object_foto');
    }
}
