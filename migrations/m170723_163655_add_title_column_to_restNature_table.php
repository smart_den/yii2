<?php

use yii\db\Migration;

/**
 * Handles adding title to table `restNature`.
 */
class m170723_163655_add_title_column_to_restNature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('restNature', 'title', $this->string(64)->after('latitude'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('restNature', 'title');
    }
}
