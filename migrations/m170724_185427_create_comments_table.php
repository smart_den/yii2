<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comments`.
 */
class m170724_185427_create_comments_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('comments', [
            'id'          => $this->primaryKey(),
            'parent_id'   => $this->smallInteger(8),
            'object_id'   => $this->smallInteger(8),
            'author_id'   => $this->smallInteger(8),
            'author_name' => $this->string(60),
            'message'     => $this->text(),
            'ip'          => $this->string(30),
            'likes'       => $this->integer(),
            'dislikes'    => $this->integer(),
            'data_create' => $this->integer(),
            'data_update' => $this->integer(),
            'status'      => $this->smallInteger(1),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('comments');
    }
}

