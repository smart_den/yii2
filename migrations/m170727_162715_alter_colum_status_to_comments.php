<?php

use yii\db\Migration;

class m170727_162715_alter_colum_status_to_comments extends Migration
{
    public function up()
    {
        $this->execute("ALTER table comments modify status ENUM('0','1') DEFAULT '0'");
        $this->execute("ALTER table comments CHANGE  `likes` `likes` INT(5) DEFAULT 0");
        $this->execute("ALTER table comments CHANGE  `dislikes` `dislikes` INT(5) DEFAULT 0");
        $this->execute("ALTER table comments CHANGE  `parent_id` `parent_id` INT(5) DEFAULT 0");
    }

    public function down()
    {
      
    }
    
}
