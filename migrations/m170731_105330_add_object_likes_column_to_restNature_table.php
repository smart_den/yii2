<?php

use yii\db\Migration;

/**
 * Handles adding object_likes to table `restNature`.
 */
class m170731_105330_add_object_likes_column_to_restNature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('restNature', 'object_likes', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('restNature', 'object_likes');
    }
}
