<?php

use yii\db\Migration;

/**
 * Handles the creation of table `likes`.
 */
class m170731_115314_create_likes_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('likes', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer(),
            'type_object' => $this->string(40),
            'type_like' => $this->integer(1),
            'id_object' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('likes');
    }
}
