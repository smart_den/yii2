<?php

use yii\db\Migration;

/**
 * Handles adding object_dislikes to table `restNature`.
 */
class m170731_123102_add_object_dislikes_column_to_restNature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('restNature', 'object_dislikes', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('restNature', 'object_dislikes');
    }
}
