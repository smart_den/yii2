<?php

use yii\db\Migration;

/**
 * Handles adding views to table `restNature`.
 */
class m170731_135350_add_views_column_to_restNature_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('restNature', 'views', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('restNature', 'views');
    }
}
