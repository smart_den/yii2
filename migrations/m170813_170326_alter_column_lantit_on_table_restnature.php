<?php

use yii\db\Migration;

class m170813_170326_alter_column_lantit_on_table_restnature extends Migration
{
    public function up()
    {
        $this->alterColumn('restNature','longitude','double NOT NULL');
        $this->alterColumn('restNature','latitude','double NOT NULL'); 
    }

    public function down()
    {

    }
}
