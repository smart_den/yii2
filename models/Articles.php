<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Articles".
 *
 * @property int $id
 * @property string $article_title
 * @property int $parent_id
 * @property string $content
 * @property string $author
 * @property string $last_update
 * @property string $create
 * @property string $hits
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_title'], 'required'],
            [['parent_id', 'hits'], 'integer'],
            [['article_title'], 'string', 'max' => 255],
            [['content', 'author', 'last_update', 'create'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_title' => 'Article Title',
            'parent_id' => 'Parent ID',
            'content' => 'Content',
            'author' => 'Author',
            'last_update' => 'Last Update',
            'create' => 'Create',
            'hits' => 'Hits',
        ];
    }

    //Функция добавляет надпись Статья к тайтлу каждой статьи
    public function getFullTitle($title)
    {
        return 'Статья ' . $title;
    }

    //фуекия делает чтобы блоки статей были одинакового размера. Если текста больше то он обрежется
    public function getShortText($text)
    {
        $text = mb_substr($text, 0, 150); // обрезаем до 150 символов
        $firsPos = strripos($text, ' '); // находим последний пробел
        $text = mb_substr($text, 0, $firsPos); // если какоето слово оканчивается на 247 символе, то обрежем по слову
        return $text . '...';

    }

    //функция принимает количество просмотров/лайков и подставляет окончания
    public function getDescription($hits, $value)
    {
        $description = array(
            'like' => array('лайк', 'лайка', 'лайков'),
            'hit' => array('просмотр', 'просмотра', 'просмотров'),
        );

        if ( mb_substr($hits, -1) == 1 && mb_substr($hits, -2) != 11){
            return $hits.' '.$description[$value][0];
        }else if ( mb_substr($hits, -1) > 1 && mb_substr($hits, -1) < 5 && ( mb_substr($hits, -2) < 5 || mb_substr($hits, -2) >15)){
            return $hits.' '.$description[$value][1];
        }else{
            return $hits.' '.$description[$value][2];
        }
    }


}
