<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "beach".
 *
 * @property int $idBeach
 * @property string $typeBeach
 */
class Beach extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'beach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typeBeach'], 'required'],
            [['typeBeach'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idBeach' => 'Id Beach',
            'typeBeach' => 'Type Beach',
        ];
    }
}
