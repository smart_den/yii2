<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

use yii\db\Expression;
use yii\base\InvalidCallException;
use yii\db\BaseActiveRecord;
use yii\db\ActiveRecord;





class Comments extends ActiveRecord
{



    public function behaviors()
    {
        return [
        'timestamp' => [
                        'class' => TimestampBehavior::className(),
                        'attributes' => [
                                    ActiveRecord::EVENT_BEFORE_INSERT => ['data_create', 'data_update'],
                                    ActiveRecord::EVENT_BEFORE_UPDATE => ['data_update'],
                                ],                        
                        ],
        ];
    }

    public static function tableName()
    {
        return 'comments';
    }

    public function rules()
    {
        return [
            [['object_id','message','author_name','author_id', 'parent_id'], 'required'],
            [['object_id', 'parent_id', 'likes', 'dislikes', 'data_create', 'data_update'], 'integer'],
            [['message','status'], 'string'],
            [['author_name'], 'string', 'max' => 60],
            [['ip'], 'string', 'max' => 30],

        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'author' => Yii::t('app', 'Author'),
            'message' => Yii::t('app', 'Message'),
            'ip' => Yii::t('app', 'Ip'),
            'likes' => Yii::t('app', 'Likes'),
            'dislikes' => Yii::t('app', 'Dislikes'),
            'data_create' => Yii::t('app', 'Data Create'),
            'data_update' => Yii::t('app', 'Data Update'),
        ];
    }
    public function createCommentsTree($array){
        echo "<div class='col-sm-12 comment_body'>";
        echo '<ul>';
        foreach ($array as $key => $value) {
            // var_dump($value);
            if($value['parent_id'] == 0 ){
                echo "<br><li>".$value['id'].$value['author_name']."<br>".$value['message']."</li><b class='comment_answer'  >Ответить</b><br><div class='form_a'></div>";
                if($value['childrens']){
                    $this->createCommentsTree($value['childrens']);
                }
            }else{
                foreach($array as $k => $v){
                  echo "<br><li>".$v['id'].$v['author_name']."<br>".$v['message']."</li><li class='lii'><b class='comment_answer'  >Ответить</b></li><br><div class='form_a'></div>";
                  // var_dump( $v);
                  if($v['childrens']){
                    $this->createCommentsTree($v['childrens']);
                  } 

                }
                break;
            }
        }
        echo '</ul>';
        echo '</div>';
    }


}
