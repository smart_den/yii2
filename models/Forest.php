<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "forest".
 *
 * @property int $idForest
 * @property string $typeTree
 */
class Forest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'forest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['typeTree'], 'required'],
            [['typeTree'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idForest' => 'Id Forest',
            'typeTree' => 'Type Tree',
        ];
    }
}
