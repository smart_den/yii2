<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "likes".
 *
 * @property int $id
 * @property int $id_user
 * @property string $type_object
 * @property int $type_like
 * @property int $id_object
 */
class Likes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'likes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'type_like', 'id_object'], 'integer'],
            [['type_object'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'type_object' => Yii::t('app', 'Type Object'),
            'type_like' => Yii::t('app', 'Type Like'),
            'id_object' => Yii::t('app', 'Id Object'),
        ];
    }
}
