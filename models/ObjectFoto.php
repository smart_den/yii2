<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "object_foto".
 *
 * @property int $id
 * @property int $object_id
 * @property string $name_img
 */
class ObjectFoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'object_foto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id'], 'integer'],
            [['name_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'object_id' => Yii::t('app', 'Object ID'),
            'name_img' => Yii::t('app', 'Name Img'),
        ];
    }
}
