<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peculiarities".
 *
 * @property int $id
 * @property int $idObject
 * @property int $forest
 * @property int $river
 * @property int $field
 * @property int $lake
 * @property int $career
 * @property int $waterfall
 * @property int $sea
 * @property int $beach
 * @property int $trenchBeam
 */
class Peculiarities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'peculiarities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idObject', 'field', 'lake', 'sea', 'trenchBeam'], 'required'],
            [['idObject', 'forest', 'river', 'field', 'lake', 'career', 'waterfall', 'sea', 'beach', 'trenchBeam'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'idObject' => Yii::t('app', 'Id Object'),
            'forest' => Yii::t('app', 'Forest'),
            'river' => Yii::t('app', 'River'),
            'field' => Yii::t('app', 'Field'),
            'lake' => Yii::t('app', 'Lake'),
            'career' => Yii::t('app', 'Career'),
            'waterfall' => Yii::t('app', 'Waterfall'),
            'sea' => Yii::t('app', 'Sea'),
            'beach' => Yii::t('app', 'Beach'),
            'trenchBeam' => Yii::t('app', 'Trench Beam'),
        ];
    }


 
}
