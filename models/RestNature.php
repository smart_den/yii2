<?php

namespace app\models;

use Yii;
use app\models\Peculiarities;

class RestNature extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'restNature';
    }

    public function rules()
    {
        return [
            [['longitude', 'latitude', 'description', 'purityEvaluation', 'purityDescription', 'beautyEvaluation', 'beautyDescription','image','food','title'], 'required'],
            [['purityEvaluation', 'beautyEvaluation', 'food', 'cost'], 'integer'],
            [['longitude', 'latitude', ],'number'],
            [['description'], 'string', 'max' => 10000],
            [['purityDescription', 'beautyDescription'], 'string', 'max' => 1000],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'description' => 'Description',
            'purityEvaluation' => 'Purity Evaluation',
            'purityDescription' => 'Purity Description',
            'beautyEvaluation' => 'Beauty Evaluation',
            'beautyDescription' => 'Beauty Description',
            'food' => 'Food',
            'cost' => 'Cost',
        ];
    }



    public function getPeculiarities()
    {
        return $this->hasMany(Peculiarities::className(), ['idObject' => 'id']);
    }

    public function afterDelete()
     {
         parent::afterDelete();
         
         $id = Yii::$app->request->get('id');
         $model = Peculiarities::find()->where(['idObject'=>$id])->one();
         $model->delete();

     }

     //функция делает чтобы блоки  были одинакового размера. Если текста больше то он обрежется
    public function getShortText($text)
    {
        $text = mb_substr($text, 0, 150); // обрезаем до 150 символов
        $firsPos = strripos($text, ' '); // находим последний пробел
        $text = mb_substr($text, 0, $firsPos); // если какоето слово оканчивается на 247 символе, то обрежем по слову
        return $text . '...';

    }

}
