<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;

class SignupForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;

    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['rememberMe', 'boolean'],
        ];
    }

    public function getUser($id = NULL)
    {   
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->email);
        }
        if ($id) {
            $this->_user = Users::findOne($id);
        }

        return $this->_user;
    }
    
    

    public function SingUp()
    {   
        if ($this->getUser()) {
            return false;
        }else{
            $user = new Users();
            return $user->addUser($this);
        }
    }
    public function Login($lastId)
    {   
        $user = $this->getUser($lastId);
        return Yii::$app->user->login($user, $this->rememberMe ? 3600*24*30 : 0);
    }
}
