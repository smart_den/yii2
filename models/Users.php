<?php

namespace app\models;

use Yii;
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['accessToken'=>$token]);
    }

    public static function findByUsername($email)
    {   
        return static::findOne(['email'=>$email]);
    }

    public function getId()
    {   
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {   
       return password_verify($password ,$this->password);
    }

    public function addUser($value)
    {
        $this->email = $value->email;
        $this->password = password_hash($value->password,PASSWORD_DEFAULT);
        $this->authKey = 'authKey'.$value->email;
        $this->accessToken = 'accessToken_'.$value->email;
        $save = $this->save();
        if ($save) {
           return Yii::$app->db->lastInsertID;
        }
        
    }
}
