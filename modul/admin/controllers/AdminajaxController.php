<?php

namespace app\modul\admin\controllers;

use Yii;
use app\controllers\AjaxController;
use yii\helpers\Json;
use app\models\Comments;

class AdminajaxController extends AjaxController
{
    public function actionAdditionallyfoto()
    {
    	$files = $_FILES['imageAdditional'];
    	$names = $files['name'];
    	$tmpNames = $files['tmp_name'];
    	$types = $files['type'];
    	$errors = $files['error'];
    	$i=0;
    	$arr = [];
    	foreach ($names as $key => $value) {
    		if ($errors[$i] == 0) {
    			$tmp = $tmpNames[$i];
	            $path = 'images/';
	            $name = $names[$i];
	            $expansion = explode('/', $types[$i])[1];
	            if ($expansion == 'jpeg' || $expansion == 'jpg' || $expansion == 'png') {
	                $fullPath = $path.md5(time()).$name;
	                if (move_uploaded_file($tmp, $fullPath)) {
	                	$arr[]= '/'.$fullPath;
	                }
	            }
    		}
    		$i++;
    	}
    	return Json::encode($arr);
    }
    public function actionDelfoto(){
    	$name = $_POST['name'];
    
    	if (file_exists(substr($name, 1))) {
    		if (unlink(substr($name, 1))) {
    			return Json::encode(true);
    		}
    	}
    }   
}  //end_class AdminajaxController 
