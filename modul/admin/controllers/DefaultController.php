<?php

namespace app\modul\admin\controllers;

use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    public $layout = 'main12';

    public function actionIndex()
    {
        return $this->redirect('restnature/');
    }
}
