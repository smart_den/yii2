<?php

namespace app\modul\admin\controllers;

use Yii;
use app\modul\admin\models\RestNature;
use app\modul\admin\models\SearchRestnature;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Users;
use app\controllers\MainController;
use app\models\Peculiarities;
use app\models\Forest;
use app\modul\admin\models\ObjectFoto;
use app\models\Beach;
use app\models\Comments;
use yii\helpers\Json;
use app\modul\admin\controllers\DefaultController;


class RestnatureController extends DefaultController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new SearchRestnature();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        //$this->view->registerJsFile('/js/googleinit.js');

        $post = Yii::$app->request->post();
        $additiFoto = $post['additiFoto'];

        $RestNature = new RestNature();
        $PeculiaritiesNature = new Peculiarities();
        $Forest = Forest::find()->asArray()->all();
        $Beach = Beach::find()->asArray()->all();

        $ArrForest['0'] = 'Выберите лес';
        foreach ($Forest as $key => $value) {
            $ArrForest[$value['idForest']] = $value['typeTree'];
        }

        $ArrBeach['0'] = 'Выберите пляж';
        foreach ($Beach as $keyBeach => $valueBeach) {
            $ArrBeach[$valueBeach['idBeach']] = $valueBeach['typeBeach'];
        }
        if ($RestNature->load($post) && $PeculiaritiesNature->load($post)) {
            if ($RestNature->save()) {
                $id = $RestNature->id;

                if ($additiFoto) {
                    Yii::$app->db->createCommand("DELETE FROM `object_foto` WHERE object_id = '".$id."'")->execute();
                    foreach ($additiFoto as $key => $foto) {
                        $model = new ObjectFoto();
                        $model->object_id = $id;
                        $model->name_img = $foto;
                        $model->save();
                    }
                }else{
                    Yii::$app->db->createCommand("DELETE FROM `object_foto` WHERE object_id = '".$id."'")->execute();
                }

                $PeculiaritiesNature->idObject = $id;
                if ($PeculiaritiesNature->save()) {

                     $this->redirect('/admin/restnature/update?id='.$id,302);   
                }
            }
         }
        return $this->render('create',compact('RestNature','PeculiaritiesNature','ArrForest','ArrBeach'));
    }

    public function actionUpdate($id)
    {
        $this->view->registerCssFile('/css/admin-object.css');

        $id = $post = Yii::$app->request->get('id');
        $post = Yii::$app->request->post();
        $additiFoto = $post['additiFoto'];

        $images = ObjectFoto::find()->where(['object_id'=>$id])->all();
        $RestNature = RestNature::findOne($id);
        $PeculiaritiesNature = Peculiarities::find()->where(['idObject'=>$id])->one();
        $Forest = Forest::find()->asArray()->all();
        $Beach = Beach::find()->asArray()->all();

        $ArrForest['0'] = 'Выберите лес';
        foreach ($Forest as $key => $value) {
            $ArrForest[$value['idForest']] = $value['typeTree'];
        }

        $ArrBeach['0'] = 'Выберите пляж';
        foreach ($Beach as $keyBeach => $valueBeach) {
            $ArrBeach[$valueBeach['idBeach']] = $valueBeach['typeBeach'];
        }

        if ($post) {
            if ($RestNature->load($post) && $PeculiaritiesNature->load($post)) {
                if ($RestNature->save()) {

                    if ($additiFoto) {

                        Yii::$app->db->createCommand("DELETE FROM `object_foto` WHERE object_id = '".$id."'")->execute();

                        foreach ($additiFoto as $key => $foto) {
                            $model = new ObjectFoto();
                            $model->object_id = $id;
                            $model->name_img = $foto;
                            $model->save();
                        }
                    }else{
                        Yii::$app->db->createCommand("DELETE FROM `object_foto` WHERE object_id = '".$id."'")->execute();
                    }
                    $PeculiaritiesNature->idObject = $id;
                    if ($PeculiaritiesNature->save()) {
      
                    }


                    $this->refresh();
                }
             }
        }
        
        return $this->render('update',compact('RestNature','PeculiaritiesNature','ArrForest','ArrBeach','images'));
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = RestNature::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
