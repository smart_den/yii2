<?php

namespace app\modul\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modul\admin\models\Comments;

/**
 * CommentsSearch represents the model behind the search form of `app\modul\admin\models\Comments`.
 */
class CommentsSearch extends Comments
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'object_id', 'parent_id', 'likes', 'dislikes', 'data_create', 'data_update','status'], 'integer'],
            [['author_name', 'message', 'ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comments::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'object_id' => $this->object_id,
            'parent_id' => $this->parent_id,
            'likes' => $this->likes,
            'dislikes' => $this->dislikes,
            'data_create' => $this->data_create,
            'data_update' => $this->data_update,
        ]);

        $query->andFilterWhere(['like', 'author_name', $this->author_name])
            ->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'ip', $this->ip]);

        return $dataProvider;
    }
}
