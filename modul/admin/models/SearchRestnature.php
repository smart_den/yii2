<?php

namespace app\modul\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modul\admin\models\RestNature;

class SearchRestnature extends RestNature
{
    public function rules()
    {
        return [
            ['title','string'],
            [['id', 'longitude', 'latitude', 'purityEvaluation', 'beautyEvaluation', 'food', 'cost'], 'integer'],
            [['description', 'purityDescription', 'beautyDescription', 'image'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RestNature::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'purityEvaluation' => $this->purityEvaluation,
            'beautyEvaluation' => $this->beautyEvaluation,
            'food' => $this->food,
            'cost' => $this->cost,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'purityDescription', $this->purityDescription])
             ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'beautyDescription', $this->beautyDescription])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
