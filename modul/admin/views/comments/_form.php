<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="comments-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'status')->radiolist(['0'=>'Not Active','1'=>'Active']) ?>
    <div><label>author_id</label>   
    <?=$model->author_id?></div>
    <div><label>author_name</label>
    <?=$model->author_name?></div>
    <div><label>parent_id</label>   
    <?=$model->parent_id?></div>
    <div><label>object_id</label>
    <?=$model->object_id?></div>
    <div><label>ip</label>
    <?=$model->ip?></div>
    <div><label>likes</label> 
    <?=$model->likes?></div>
    <div><label>dislikes</label>
    <?=$model->dislikes?></div>
    <div><label>data_update</label>
    <?=$model->data_update?></div>
    <div><label>data_create</label>
    <?=$model->data_create?></div>
    <div><label>message</label>
    <?=$model->message?></div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
