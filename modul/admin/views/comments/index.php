<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('app', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comments-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?//= Html::a(Yii::t('app', 'Create Comments'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div style="width: 900px;">
<?php Pjax::begin(['id' => 'comments']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'object_id',
            'status',
            'parent_id',
            'author_name',
            'message:ntext',
             'ip',
             'likes',
             'dislikes',
             'data_create',
             'data_update',
             [  
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:100px;'],
                'header'=>'Edit Status',
                'template' => '{active} {notactive}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        if ($model->status == '1') {
                            $style = 'style="color:green"';
                        }
                        $url = $_SERVER['REQUEST_URI'];
                       if ($_GET) {
                        $path = parse_url($url)['path'];
                        $query = parse_url($url)['query'];
                        if (!strripos($path,'/status')) {
                            if ($_GET['page']) {
                                $url = str_replace('index','status',$url);
                            }else{
                               $path .= '/status/';
                                $url = $path.'?'.$query; 
                            }
                        }
                                if (!$_GET['id']) {
                                    $url = $url.'&id='.$model->id;
                                }else{
                                    $url = preg_replace('/id=*\d{1,100}/','id='.$model->id,$url);
                                }
                                if(!$_GET['status']){
                                    $url = $url.'&status=active';
                                }else{
                                    $url = str_replace('status=notactive','status=active',$url);
                                }
                                
                                return '<div class="buttons_comments"><a href="'.$url.'"><span '.$style.' class="glyphicon glyphicon-ok"></span></a>';
                            }else{
                                $dd = $url.'/status/?id='.$model->id.'&status=active';
                                return '<div class="buttons_comments"><a href="'.$dd.'"><span '.$style.' class="glyphicon glyphicon-ok"></span></a>';
                            }
                    },
                    'notactive' => function($url, $model){
                        $url = $_SERVER['REQUEST_URI'];
                        if ($model->status == '0') {
                            $style = 'style="color:red"';
                        }
                            if ($_GET) {
                                $path = parse_url($url)['path'];
                            $query = parse_url($url)['query'];
                            if (!strripos($path,'/status')) {
                               if ($_GET['page']) {
                                    $url = str_replace('index','status',$url);
                                }else{
                                   $path .= '/status/';
                                    $url = $path.'?'.$query; 
                                }
                            }
                                if (!$_GET['id']) {
                                    $url = $url.'&id='.$model->id;
                                }else{
                                    $url = preg_replace('/id=*\d{1,100}/','id='.$model->id,$url);
                                }
                                if(!$_GET['status']){
                                    $url = $url.'&status=notactive';
                                }else{
                                    $url = str_replace('status=active','status=notactive',$url);
                                }
                                return '<a href="'.$url.'"><span '.$style.' class="glyphicon glyphicon-remove"></span></a></div>';
                            }else{
                                $dd = $url.'/status/?id='.$model->id.'&status=notactive';
                                return '<a href="'.$dd.'"><span '.$style.' class="glyphicon glyphicon-remove"></span></a></div>';
                            }
                    }
                ],

            ],
            [  
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:100px;'],
                'header'=>'Actions',
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<div class="buttons_comments"><span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Update'),                                
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => '',
                            'data' => [
                                'confirm' => 'Are you absolutely sure ? ',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],

            ],
        ],
    ]); ?>
    <?php Pjax::end() ?>
    </div>
</div>
