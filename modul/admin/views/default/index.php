<?
use yii\helpers\Html;

?>
<div class="admin-default-index">
    <h1>Admin Panel</h1>
    <div><?= Html::a('Rest Nature', ['/admin/restnature']) ?></div>
    <div><?= Html::a('Comments', ['/admin/comments']) ?></div>
</div>
