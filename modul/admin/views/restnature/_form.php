<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$post = Yii::$app->request->post();

if ($post['PeculiaritiesNature']['river']) {
    $PeculiaritiesNature->river  = $post['PeculiaritiesNature']['river'];
}
if ($post['PeculiaritiesNature']['career']) {
    $PeculiaritiesNature->career  = $post['PeculiaritiesNature']['career'];
}
if ($post['PeculiaritiesNature']['lake']) {
    $PeculiaritiesNature->lake  = $post['PeculiaritiesNature']['lake'];
}
if ($post['PeculiaritiesNature']['waterfall']) {
    $PeculiaritiesNature->waterfall  = $post['PeculiaritiesNature']['waterfall'];
}
if ($post['PeculiaritiesNature']['field']) {
    $PeculiaritiesNature->field  = $post['PeculiaritiesNature']['field'];
}
if ($post['PeculiaritiesNature']['sea']) {
        $PeculiaritiesNature->sea  = $post['PeculiaritiesNature']['sea'];
}
if ($post['PeculiaritiesNature']['forest']) {
    $PeculiaritiesNature->forest  = $post['PeculiaritiesNature']['forest'];
}
if ($post['PeculiaritiesNature']['trenchBeam']) {
    $PeculiaritiesNature->trenchBeam  = $post['PeculiaritiesNature']['trenchBeam'];
}
if ($post['PeculiaritiesNature']['beach']) {
    $PeculiaritiesNature->beach  = $post['PeculiaritiesNature']['beach'];
}
?>
<div>
    <div class="row">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'form_addobj']])?>
        <div class="col-sm-6">
            <div><?=$form->field($RestNature, 'title')->input('text')?></div>
            <?=$form->field($RestNature, 'longitude')->hiddenInput(['id'=>'google2'])->label('Укажите место положение')?>
            <?=$form->field($RestNature, 'latitude')->hiddenInput(['id'=>'google1'])->label('')?>
            <input type="text" name="" id="search" class="form-control searchGoogle">
            <div id="map" style="width: 500px; height: 400px;"></div>
            <div><?=$form->field($RestNature, 'purityEvaluation')->hiddenInput(['id'=>'star2_input_1'])->label('')?></div>
            <div class='starrr' id='star2_1'></div>
            <div><?=$form->field($RestNature, 'purityDescription')->textarea(['rows' => '2'])?></div>
            <div><?=$form->field($RestNature, 'beautyEvaluation')->hiddenInput(['id'=>'star2_input_2'])?></div>
            <div class='starrr' id='star2_2'></div>
            <div><?=$form->field($RestNature, 'beautyDescription')->textarea(['rows' => '2'])?></div>
            <div class="block_food"><?= $form->field($RestNature, 'food')->radioList([1 => 'yes', 0 => 'No'])->label('Food');?></div>
            <div class="block_forest">
                <span  class="checkbox checkbox-info col-sm-2 test_sre">
                    <input <?if($PeculiaritiesNature->river){echo 'checked';}?> type="checkbox" name="river" class="checbox_hide checkbox-circle" value="river_checkbox" id="riverchek">
                    <label for="riverchek">River</label>
                </span>
                <span class="checkbox checkbox-info col-sm-2 test_sre "">
                    <input <?if($PeculiaritiesNature->career){echo 'checked';}?> type="checkbox" name="" class="checbox_hide" value="career_checkbox" id="careerchek">
                    <label for="careerchek">career</label>
                </span>
                <span class="checkbox checkbox-info col-sm-2 test_sre"">
                    <input <?if($PeculiaritiesNature->lake){echo 'checked';}?> type="checkbox" name="" class="checbox_hide" value="lake_checkbox" id="lakechek">
                    <label for="lakechek">lake</label>
                </span>
                <span class="checkbox checkbox-info col-sm-2 test_sre"">
                    <input <?if($PeculiaritiesNature->waterfall){echo 'checked';}?> type="checkbox" name="" class="checbox_hide" value="waterfall_checkbox" id="waterfallchek">
                    <label for="waterfallchek">waterfall</label>
                </span>
                <span class="checkbox checkbox-info col-sm-2 test_sre"">
                    <input <?if($PeculiaritiesNature->beach){echo 'checked';}?> type="checkbox" name="" class="checbox_hide" value="beach_checkbox" id="beachchek">
                    <label for="beachchek">beach</label>
                </span>
                <span class="checkbox checkbox-info col-sm-2 test_sre"">
                    <input <?if($PeculiaritiesNature->forest){echo 'checked';}?> type="checkbox" name="" class="checbox_hide" value="forest_checkbox" id="forestchek">
                    <label for="forestchek">forest</label>
                </span>
            </div>
            <div <?if(!$PeculiaritiesNature->river){echo 'hidden';}?> id="river_checkbox">
                <?=$form->field($PeculiaritiesNature, 'river')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?>
            </div>
            <div <?if(!$PeculiaritiesNature->career){echo 'hidden';}?> id="career_checkbox">
                <div><?=$form->field($PeculiaritiesNature, 'career')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
            </div>
            <div <?if(!$PeculiaritiesNature->lake){echo 'hidden';}?> id="lake_checkbox">
                <div><?=$form->field($PeculiaritiesNature, 'lake')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
            </div>
            <div <?if(!$PeculiaritiesNature->waterfall){echo 'hidden';}?> id="waterfall_checkbox">
                <div><?=$form->field($PeculiaritiesNature, 'waterfall')->dropDownList(['0'=>'Выбрать Высоту','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра','5'=>'5 Метров'])?></div>
            </div>
            <div <?if(!$PeculiaritiesNature->forest){echo 'hidden';}?> id="forest_checkbox">
                <?=$form->field($PeculiaritiesNature, 'forest')->dropDownList($ArrForest);?>
            </div>
            <div <?if(!$PeculiaritiesNature->beach){echo 'hidden';}?> id="beach_checkbox">
                <?=$form->field($PeculiaritiesNature, 'beach')->dropDownList($ArrBeach);?>
            </div>
            <div class="row block_sea_lake">
                <span class="col-sm-3">
                    <?=$form->field($PeculiaritiesNature, 'trenchBeam')->checkbox(['value'=>'1','label'=>'Балка'])?>
                </span>
                <span class="col-sm-3">
                    <?=$form->field($PeculiaritiesNature, 'field')->checkbox(['value'=>'1','label'=>'Поле'])?>
                </span>
                <span class="col-sm-3">
                    <?=$form->field($PeculiaritiesNature, 'sea')->checkbox(['value'=>'1','label'=>'Море'])?>
                </span>
            </div>
            <div><?=$form->field($RestNature, 'description')->textarea(['rows' => '6'])?></div>
            <div><?=Html::submitButton('Отправить', ['class' => 'btn btn-primary'])?></div>
        </div>
        <div class="col-sm-3">
            <div class="mainfoto">
                <?if(file_exists(substr($RestNature->image,1))){
                    echo $form->field($RestNature, 'image')->hiddenInput(['id'=>'imagehide']);
                }else{
                    echo $form->field($RestNature, 'image')->hiddenInput(['id'=>'imagehide','value'=>'']);
                }?>
                <input type="file" name="image" id="imageMain" style="display:none">
                <div id="addfotoid1" class="btn btn-info">Добавить главное фото</div>
                <div class="result"></div>
                <a href="#" class="delete_image btn btn-danger" onclick="clickDelimage();" style="display:none">Удалить</a>
            </div>
            <div class="line_foto"></div>
            <div class="block_fotos">
                <input type="file" name="imageAdditional[]" id="imageMainAdd" multiple="" style="display:none">
                <div id="addfotoid2" class="btn btn-info">Добавить доп. фото</div>
            </div>
            <div id="main_block_add_img">
                <?
                if ($images) {
                foreach ($images as $key => $img) {?>
                    <div>
                        <input type="text" hidden name="additiFoto[]" value="<?=$img->name_img?>">
                        <img src="<?=$img->name_img?>" class="img_dop">
                        <p class="del_img glyphicon glyphicon-remove"></p>
                    </div>
                <?}}?>
            </div>
        </div>
        <?php ActiveForm::end()?>
    </div>
</div>




