<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modul\admin\models\SearchRestnature */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rest-nature-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?//= $form->field($model, 'longitude') ?>

    <?//= $form->field($model, 'latitude') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'purityEvaluation') ?>

    <?php // echo $form->field($model, 'purityDescription') ?>

    <?php // echo $form->field($model, 'beautyEvaluation') ?>

    <?php // echo $form->field($model, 'beautyDescription') ?>

    <?php // echo $form->field($model, 'food') ?>

    <?php // echo $form->field($model, 'cost') ?>

    <?php // echo $form->field($model, 'image') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
