<?php

use yii\helpers\Html;
use yii\grid\GridView;

$this->title = Yii::t('app', 'Rest Natures');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rest-nature-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php//  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Rest Nature'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<div class="grid_admin">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            //'longitude',
            //'latitude',
            //'description',
            'purityEvaluation',
            // 'purityDescription',
             'beautyEvaluation',
            // 'beautyDescription',
            // 'food',
            // 'cost',
            // 'image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
