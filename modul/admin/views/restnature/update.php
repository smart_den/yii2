<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Rest Nature',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rest Natures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rest-nature-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form',compact('RestNature','PeculiaritiesNature','ArrForest','ArrBeach','images')) ?>

</div>
<script type="text/javascript">
var marker1;
function myMap() {
  var icon = {
              url: '/images/tree.png',
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

  var mapCanvas = document.getElementById("map");
  var lontit = document.getElementById("google2");
  var lantit = document.getElementById("google1");
  if (lontit.value && lantit.value) {
    var myCenter = new google.maps.LatLng(lantit.value,lontit.value);
  }else{
    var myCenter = new google.maps.LatLng(0,0);
  }  

  var mapOptions = {center: myCenter, zoom: 10};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  marker1 = new google.maps.Marker({
        position: myCenter,
        map: map,
        icon:icon,
      });

  var input = document.getElementById('search');
  var searchBox = new google.maps.places.SearchBox(input);
  
   map.addListener('bounds_changed', function() {
     searchBox.setBounds(map.getBounds());
   });

        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }
          
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
 
            if (marker1) {marker1.setMap(null)}

             var icon = {
              url: '/images/tree.png',
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

              marker1 = new google.maps.Marker({
              map: map,
              icon:icon,
              title: place.name,
              position: place.geometry.location
            });

          	var input = document.getElementById("google1");
	  		input.value = marker1.position.lat();
	  		var input = document.getElementById("google2");
	  		input.value = marker1.position.lng(); 

            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
     
  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(map, event.latLng);
  });
}

function placeMarker(map, location) {

	var icon = {
              url: '/images/tree.png',
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

	if (!marker1) {
		var marker = new google.maps.Marker({
		    position: location,
		    map: map,
		    icon:icon,
  		});
  		marker1 = marker;

  		var input = document.getElementById("google1");
  		input.value = location.lat();
  		var input = document.getElementById("google2");
  		input.value = location.lng(); 

	}else{
		marker1.setMap(null);
		var marker = new google.maps.Marker({
		    position: location,
		    map: map,
		    icon:icon
  		});
  		marker1 = marker;

  		var input = document.getElementById("google1");
  		input.value = location.lat();
  		var input = document.getElementById("google2");
  		input.value = location.lng(); 
	}
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcTuaWLvC6UDj_K8Xta7wbtf_7cEjEsCA&callback=myMap&libraries=places"></script>
