<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectComments */
/* @var $form ActiveForm */
?>
<div class="object">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'object_id') ?>
        <?= $form->field($model, 'parent_id') ?>
        <?= $form->field($model, 'likes') ?>
        <?= $form->field($model, 'dislikes') ?>
        <?= $form->field($model, 'message') ?>
        <?= $form->field($model, 'data_create') ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'author') ?>
        <?= $form->field($model, 'ip') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- object -->
