<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$post = Yii::$app->request->post();
?>
<div>
    <div>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'form_addobj']])?>
        <?= $form->field($RestNature, 'image')->hiddenInput(['id'=>'imagehide'])?>
        <input type="file" name="image" id="imageMain">
        <div class="result"></div>
        <a href="#" class="delete_image" onclick="clickDelimage();" hidden>Удалить</a>
        <?= Html::hiddenInput('nameImage', '',['id'=>'imageName'])?>
        <div><?=$form->field($RestNature, 'longitude')->input('text')?></div>
        <div><?=$form->field($RestNature, 'latitude')->input('text')?></div>
        <div><?=$form->field($RestNature, 'description')->textarea(['rows' => '6'])?></div>
        <div><?=$form->field($RestNature, 'purityEvaluation')->hiddenInput(['id'=>'star2_input_1'])?></div>
        <div class='starrr' id='star2_1'></div>
        <div><?=$form->field($RestNature, 'purityDescription')->textarea(['rows' => '2'])?></div>
        <div><?=$form->field($RestNature, 'beautyEvaluation')->hiddenInput(['id'=>'star2_input_2'])?></div>
        <div class='starrr' id='star2_2'></div>
        <div><?=$form->field($RestNature, 'beautyDescription')->textarea(['rows' => '2'])?></div>
        <div><?= $form->field($RestNature, 'food')->radioList([1 => 'yes', 0 => 'No'])->label('Food');?></div>
        <?
        if ($post['PeculiaritiesNature']['river']) {
            $PeculiaritiesNature->river  = $post['PeculiaritiesNature']['river'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'river')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['PeculiaritiesNature']['career']) {
            $PeculiaritiesNature->career  = $post['PeculiaritiesNature']['career'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'career')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['PeculiaritiesNature']['lake']) {
            $PeculiaritiesNature->lake  = $post['PeculiaritiesNature']['lake'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'lake')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['PeculiaritiesNature']['field']) {
            $PeculiaritiesNature->field  = $post['PeculiaritiesNature']['field'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'field')->checkbox(['value'=>'1','label'=>'Поле'])?></div>
        <?
        if ($post['PeculiaritiesNature']['waterfall']) {
            $PeculiaritiesNature->waterfall  = $post['PeculiaritiesNature']['waterfall'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'waterfall')->dropDownList(['0'=>'Выбрать Высоту','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра','5'=>'5 Метров'])?></div>
        <?
        if ($post['PeculiaritiesNature']['sea']) {
            $PeculiaritiesNature->sea  = $post['PeculiaritiesNature']['sea'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'sea')->checkbox(['value'=>'1','label'=>'Море'])?></div>
        <?
        if ($post['PeculiaritiesNature']['trenchBeam']) {
            $PeculiaritiesNature->trenchBeam  = $post['PeculiaritiesNature']['trenchBeam'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'trenchBeam')->checkbox(['value'=>'1','label'=>'Балка'])?></div>
        <?
        if ($post['PeculiaritiesNature']['forest']) {
            $PeculiaritiesNature->forest  = $post['PeculiaritiesNature']['forest'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'forest')->dropDownList($ArrForest);?></div>
        <?
        if ($post['PeculiaritiesNature']['beach']) {
            $PeculiaritiesNature->beach  = $post['PeculiaritiesNature']['beach'];
        }?>
        <div><?=$form->field($PeculiaritiesNature, 'beach')->dropDownList($ArrBeach);?></div>
        <div><?=Html::submitButton('Отправить', ['class' => 'btn btn-primary'])?></div>
        
        <?php ActiveForm::end()?>
    </div>
</div>




