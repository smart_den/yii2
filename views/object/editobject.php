<?
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$post = Yii::$app->request->post();
?>
<div>
    <div>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'form_addobj']])?>
        <?= $form->field($RestNature, 'image')->hiddenInput(['id'=>'imagehide'])?>
        <input type="file" name="image" id="imageMain">
        <div class="result">
            <img src="<?=$RestNature->image?>">
        </div>
        <?=$form->field($Peculiarities, 'idObject')->hiddenInput(['value'=>$Peculiarities->idObject])->label('')?>
        <a href="#" class="delete_image" onclick="clickDelimage();" hidden>Удалить</a>
        <div><?=$form->field($RestNature, 'longitude')->input('text')?></div>
        <div><?=$form->field($RestNature, 'latitude')->input('text')?></div>
        <div><?=$form->field($RestNature, 'description')->textarea(['rows' => '6'])?></div>
        <div><?=$form->field($RestNature, 'purityEvaluation')->hiddenInput(['id'=>'star2_input_1'])?></div>
        <div class='starrr' id='star2_1'></div>
        <div><?=$form->field($RestNature, 'purityDescription')->textarea(['rows' => '2'])?></div>
        <div><?=$form->field($RestNature, 'beautyEvaluation')->hiddenInput(['id'=>'star2_input_2'])?></div>
        <div class='starrr' id='star2_2'></div>
        <div><?=$form->field($RestNature, 'beautyDescription')->textarea(['rows' => '2'])?></div>
        <div><?= $form->field($RestNature, 'food')->radioList([1 => 'yes', 0 => 'No'])->label('Food');?></div>
        <?
        if ($post['Peculiarities']['river']) {
            $Peculiarities->river  = $post['Peculiarities']['river'];
        }?>
        <div><?=$form->field($Peculiarities, 'river')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['Peculiarities']['career']) {
            $Peculiarities->career  = $post['Peculiarities']['career'];
        }?>
        <div><?=$form->field($Peculiarities, 'career')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['Peculiarities']['lake']) {
            $Peculiarities->lake  = $post['Peculiarities']['lake'];
        }?>
        <div><?=$form->field($Peculiarities, 'lake')->dropDownList(['0'=>'Выбрать глубину','1'=>'1 Метр','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра'])?></div>
        <?
        if ($post['Peculiarities']['field']) {
            $Peculiarities->field  = $post['Peculiarities']['field'];
        }?>
        <div><?=$form->field($Peculiarities, 'field')->checkbox(['value'=>'1','label'=>'Поле'])?></div>
        <?
        if ($post['Peculiarities']['waterfall']) {
            $Peculiarities->waterfall  = $post['Peculiarities']['waterfall'];
        }?>
        <div><?=$form->field($Peculiarities, 'waterfall')->dropDownList(['0'=>'Выбрать Высоту','2'=>'2 Метра','3'=>'3 Метра','4'=>'4 Метра','5'=>'5 Метров'])?></div>
        <?
        if ($post['Peculiarities']['sea']) {
            $Peculiarities->sea  = $post['Peculiarities']['sea'];
        }?>
        <div><?=$form->field($Peculiarities, 'sea')->checkbox(['value'=>'1','label'=>'Море'])?></div>
        <?
        if ($post['Peculiarities']['trenchBeam']) {
            $Peculiarities->trenchBeam  = $post['Peculiarities']['trenchBeam'];
        }?>
        <div><?=$form->field($Peculiarities, 'trenchBeam')->checkbox(['value'=>'1','label'=>'Балка'])?></div>
        <?
        if ($post['Peculiarities']['forest']) {
            $Peculiarities->forest  = $post['Peculiarities']['forest'];
        }?>
        <div><?=$form->field($Peculiarities, 'forest')->dropDownList($ArrForest);?></div>
        <?
        if ($post['Peculiarities']['beach']) {
            $Peculiarities->beach  = $post['Peculiarities']['beach'];
        }?>
        <div><?=$form->field($Peculiarities, 'beach')->dropDownList($ArrBeach);?></div>
        <div><?=Html::submitButton('Отправить', ['class' => 'btn btn-primary'])?></div>
        
        <?php ActiveForm::end()?>
    </div>
</div>




