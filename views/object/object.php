
<div class="container">
    <?php

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model app\models\LoginForm */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use app\models\Comments;


$this->title = 'object';
$this->params['breadcrumbs'][] = $this->title;
?>


<h2><?=$object->title ?></h2><br>
<input type="hidden" value="<?=$object->id?>" id="id_object">
<div class="fotorama col-sm-12" data-nav="thumbs">
    <img src="<?php echo $object->image ?>" class="main_img"  alt="image"> 
    <? foreach($object_photo as $v){?>    
           <a href="<?=$v->name_img ?>"> <img src="<?=$v->name_img ?>" class="main_img"  alt="image"></a> 
    <?}?>
</div>
<div>
    <div id="likes_object">
    <?if(Yii::$app->user->isGuest){?>
            <button class="button_like_guest"><span class="glyphicon glyphicon-thumbs-up"></span> Likes <span class="count_likes"><?=$object->object_likes?></span></button>
        <?}else{
            if ($user['like'] == 1) {?>
                <button class="button_like_del"><span class="glyphicon glyphicon-thumbs-up"></span> Likes <span class="count_likes"><?=$object->object_likes?></span></button>
            <?}else{?>
                <button class="button_like_add"><span class="glyphicon glyphicon-thumbs-up"></span> Likes <span class="count_likes"><?=$object->object_likes?></span></button>
            <?}}?>
    </div>
    <div id="dislikes_object">
    <?if(Yii::$app->user->isGuest){?>
            <button id="dislikes_object"><span class="glyphicon glyphicon-thumbs-down"></span> Dis Likes <span class="count_dislikes"><?=$object->object_dislikes?></span></button>
        <?}else{
            if ($user['dislike'] == 1) {?>
                <button class="button_dislike_del"><span class="glyphicon glyphicon-thumbs-down"></span> Dis Likes <span class="count_dislikes"><?=$object->object_dislikes?></span></button>
            <?}else{?>
                <button class="button_dislike_add"><span class="glyphicon glyphicon-thumbs-down"></span> Dis Likes <span class="count_dislikes"><?=$object->object_dislikes?></span></button>
            <?}}?>
    </div>
    <div>
        <span>Views : </span><?=$object->views?>
    </div>
</div>
<div class="row col-sm-12">
    <p><?=$object->description ?><br></p>
</div>

<div class="row col-sm-12">
    <p><b>Координаты объекта : Ширина - <?=$object->latitude?> Долгота - <?=$object->longitude?></b></p>
</div>
<div class="row col-sm-12">
    <p><b>Чистота: <?=$object->purityEvaluation?></b><br>
            <?=$object->purityDescription?></p>
</div>

<div class="row col-sm-12">
    <p><b>Красота: <?=$object->beautyEvaluation?></b><br>
            <?=$object->beautyDescription?></p>
</div>

<table  cellpadding="7" border="2" width="100%" align="center" > 
    <tr>
        <td align="center"><b>Еда</b></td>
        <td align="center"><b>Цены</b></td>
        <td align="center"><b>Лес</b></td>
        <td align="center"><b>Пляж</b></td>
        <td align="center"><b>Река</b></td>
        <td align="center"><b>Поле</b></td>
        <td align="center"><b>Озеро</b></td>
        <td align="center"><b>Карьер</b></td>
        <td align="center"><b>Водопад</b></td>
        <td align="center"><b>Море</b></td>
        <td align="center"><b>Балка</b></td>
    </tr>
        <td align="center"><b><?=$object->food ?></b></td>
        <td align="center"><b><?=$object->cost ?></b></td>
        <td align="center"><b><?=$forest->typeTree ?></b></td>
        <td align="center"><b><?=$beach->typeBeach ?></b></td>
        <td align="center"><b><?=$peculiarities->river ?></b></td>
        <td align="center"><b><?=$peculiarities->field ?></b></td>
        <td align="center"><b><?=$peculiarities->lake ?></b></td>
        <td align="center"><b><?=$peculiarities->career ?></b></td>
        <td align="center"><b><?=$peculiarities->waterfall ?></b></td>
        <td align="center"><b><?=$peculiarities->sea ?></b></td>
        <td align="center"><b><?=$peculiarities->trenchBeam ?></b></td>
    <tr>        
    </tr>
</table>

<a href="<?=Url::to('showobjects')?>">К списку объектов</a><br>  

<!-- <span class='make_comment'  data-comment = '0' ><b>Оставить комментарий</b></span> -->
<!-- <div id="body_main_comment" class="col-sm-12"></div> -->
 <div class="col-sm-12 comment comment_obj" id="main_comment">
        <h4>Оставить комментарий:</h4>     
        <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data', 'name' => 'form_comment', 'class'=> 'object_comment_class', 'onsubmit'=> 'return false']]);?>
         
        <?= $form->field($model, 'object_id')->hiddenInput(['value' => $object->id])->label(false);?>
        <?= $form->field($model, 'parent_id')->hiddenInput(['value'=>0])->label(false);?>
        <? if(Yii::$app->user->isGuest){
          echo $form->field($model, 'author_name')->textInput(['class'=>'author_name_obj'])->label('Имя');
        }else {
             echo $form->field($model, 'author_name')->hiddenInput(['class'=>'author_name_obj'])->label(false);
        }
        ?>      
        <?= $form->field($model, 'message')->textarea(['class'=>'message_obj' ,'rows'=>5, 'value' => $comments->content])->label('Текст сообщения') ?>      

        <div class="form-group">
            <?= Html::submitButton('Отправить', [ 'class' => 'send_comment btn btn-success' ])?>
        </div>

        <?php ActiveForm::end();?>
</div>
<?php
// print_r($object);->date
// print_r($object_photo);
// print_r($peculiarities);
// print_r($Forest);
// print_r($Beach);

?>

<div class="row" id='row_obj'>
   

    <div id ="comments" class="col-sm-12">
        <h4>Комментарии: <?=$count?></h4>
        <div id="comments_body">
              <? $model->createCommentsTree($tree) ?>
        </div>        
    </div>


</div>

                <!-- Modal -->
                <div class="modal " id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title" id="good_msg">Ваш комментарий добавлен</h4>
                          <h4 class="modal-title"  hidden id="error_msg">Ваш комментарий нихуя не добавлен.</h4>
                      </div>
                      
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                      </div>
                  </div>
                </div>
</div>
</div>







  <div class="col-sm-12 comment comment_obj_1 answer_comment"  hidden>   
                    <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data', 'name' => 'form_comment', 'class'=> 'object_comment_class', 'onsubmit'=> 'return false']]);?>
                    <span class="close_form">закрыть</span>
                    <?= $form->field($model, 'object_id')->hiddenInput(['value' => $object->id])->label(false);?>
                    <?= $form->field($model, 'parent_id')->hiddenInput(['value'=>$comment->id])->label(false);?>
                    <? if(Yii::$app->user->isGuest){
                      echo $form->field($model, 'author_name')->textInput(['class'=>'author_name_obj'])->label('Имя');
                  }else {
                     echo $form->field($model, 'author_name')->hiddenInput(['class'=>'author_name_obj'])->label(false);
                 }
                 ?>      
                 <?= $form->field($model, 'message')->textarea(['class'=>'message_obj' ,'rows'=>5])->label('Текст сообщения') ?>      

                 <div class="form-group">
                    <?= Html::submitButton('Отправить', [ 'class' => 'send_comment btn btn-success' ])?>
                 </div>

                <?php ActiveForm::end();?>
  </div> 















