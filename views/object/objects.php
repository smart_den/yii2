<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'show';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-12">
            <div>
                <h2>Объекты</h2>
            </div>
            <div class="row">
                <?php foreach($objects as $object) {?>
                        <div class="col-sm-4 object">
                            <div class="">                 
                                <div class="main_block_object">
                                    <a href="<?=Url::to([ 'showobject', 'id'=>$object->id])?>"><img src="<?php echo $object->image ?>" class="main_img"  alt="image"></a>
                                </div>
                                <div class="up_score">
                                    <a href="<?=Url::to([ 'showobject', 'id'=>$object->id])?>"><b><?php echo $object->title ?></b></a>
                                </div>
                                <p><?php echo $object->description ?></p>                                      
                                <a href="<?=Url::to([ 'showobject', 'id'=>$object->id])?>"> <b>Подробнее...</b> </a>
                                <div class="block_likes_objects">
                                    <span class="glyphicon glyphicon-thumbs-up"><span> Likes </span><?=$object->object_likes?> </span>
                                    <span class="glyphicon glyphicon-thumbs-down margin_likes"><span> Dis Likes </span><?=$object->object_dislikes?> </span>
                                    <span class="glyphicon glyphicon-eye-open margin_likes"><span> Views </span><?=$object->views?> </span>
                                </div>
                            </div>
                        </div>
                <?php  } ?>
            </div>
            <div class="pagination"><?= LinkPager::widget(['pagination' => $pagination]); ?></div>
        </div>
    </div> 
</div>



