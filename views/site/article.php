<div class="container">
    <?php

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model app\models\LoginForm */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use app\models\Comments;
    use app\models\Articles;

    $this->title = 'article';
    $this->params['breadcrumbs'][] = $this->title;
    ?>


<?php
//echo '<pre>';
//print_r($article);
//print_r($Forest);
//print_r($Beach);

// Создаем модель коментариев
$model = new Comments;

?>


        <h1><?= $article->getFullTitle($article->article_title)?></h1>
        <?= $article->article_title?><br>
        <?= $article->content?><br>
        <?= $article->author?><br>
        <?//= $article->getDescription($article->likes, 'like')?><!--<br>-->
        <?= $article->getDescription($article->hits, 'hit')?><br>
        <a href="<?=Url::to('articles')?>">К списку статей</a>


            <div class="row">
                <div class="col-sm-12 comment">
                    <h4>Оставить комментарий:</h4>
                    <?php if(Yii::$app->session->hasFlash('success')){?>
                        <div class="alert alert-success" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="close"></button>
                            <strong><?= Yii::$app->session->getFlash('success');?></strong>
                        </div>

                    <?php } ?>
                    <?php $form = ActiveForm::begin( ['options' => ['enctype' => 'multipart/form-data', 'name' => 'form_name', 'class'=> 'some_class'], 'id' => 'edit-form']);?>


                        <?= $form->field($model, 'parent_id_comment')->hiddenInput(['value' => $article->id])->label(false);?>
                        <?= $form->field($model, 'comment_author')->textInput(['value' => $comments->name])->label('Имя') ?>
                        <?= $form->field($model, 'message')->textarea(['rows'=>5, 'value' => $comments->content])->label('Текст сообщения') ?>
                        <?= $form->field($model, 'img')->fileInput()->label('Фото') ?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-success'])?>
                    </div>

                    <?php ActiveForm::end();?>
                </div>
                <div class="col-sm-12 ">
                    <h4>Комментарии:</h4>
                    <?php foreach($comments as $comment){?>

                        <p><b><?=$comment->comment_author.'</b>  '.$comment->data_create?></p>
                        <p><?=$comment->message?></p>
                        <p><?=$comment->img?></p>
                        <p><img src="/images/<?=$comment->img?>" alt=""></p>
                      <!--   <?= Html::img(Yii::$app->urlManagerBackend->createUrl($comment->img)) ?>
                        <?= Html::img(Yii::$app->urlManager->createUrl($comment->img)) ?> -->
                    <?php }?>
                </div>
            </div>


</div>

