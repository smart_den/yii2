<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<?
$session = Yii::$app->session;
$flashsess = $session->getFlash('EmailExists');
if ($flashsess) {?>
	<div class="alert alert-danger">
  		<strong>Внимание!</strong> <?=$flashsess?>
	</div>
<?}?>
<div>
	<div>
		<div>
			<?php $form = ActiveForm::begin()?>
			<div><?=$form->field($registerform, 'email')->input('email')?></div>
			<div><?=$form->field($registerform, 'password')->passwordInput()?></div>
			<div><?=Html::submitButton('Отправить', ['class' => 'submit'])?></div>
			<?php ActiveForm::end()?>
		</div>
	</div>
</div>