addfoto();

function addfoto(){
  $('#imageMain').change(function(e){
    delFoto($('#imagehide').val());
    var $that = $('#form_addobj');
    e.preventDefault();
    formData = new FormData($that.get(0)); 
    $.ajax({
      url: '/ajax/addfoto',
      type: $that.attr('method'),
      contentType: false, // важно - убираем форматирование данных по умолчанию
      processData: false, // важно - убираем преобразование строк по умолчанию
      data: formData,
      dataType: 'json',
      success: function(json){
        if(json){
          $('.result').html('<img src="'+json+'">');
          // $('.delete_image').show();
           $('#addfotoid1').text('Изменить главное фото');
           $('#imagehide').val(json);
        }
      }
    });
  });
}

function delFoto(path){ 
    $.ajax({
      url: '/admin/adminajax/delfoto',
      type: 'post',
      data: 'name='+path,
      success: function(json){
        if (json) {
          return true;
        }
      }
    });
}


(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-39205841-5', 'dobtco.github.io');
    ga('send', 'pageview');

    var $s2input = $('#star2_input_1');
    $('#star2_1').starrr({
      max: 10,
      rating: $s2input.val(),
      change: function(e, value){
      	$('#star2_input_1').val(value);
      }
    });

    var $s2input = $('#star2_input_2');
    $('#star2_2').starrr({
      max: 10,
      rating: $s2input.val(),
      change: function(e, value){
        $('#star2_input_2').val(value);
      }
    });

$(document).ready(function(){    
  if($('#imagehide').val()){
    var image_path = $('#imagehide').val();
    $('.result').html('<img src="'+image_path+'">');
    $('#addfotoid1').text('Изменить главное фото');
    //$('.delete_image').show();
  }
});

$('.checbox_hide').click(function(e){
   var block = e.target;
   if (block.checked) {
     var id = '#'+block.value;
     $(id).show();
   }else{
     var id = '#'+block.value;
     $(id).children().find('select').val(0);
     $(id).hide();
   }
});


var time1;
additionallyFoto();

function additionallyFoto(){
    $('#imageMainAdd').change(function(e){
      clearTimeout(time1);
      time1 = setTimeout(function(){
      var $that = $('#form_addobj');
      e.preventDefault();
      formData = new FormData($that.get(0)); 
      $.ajax({
        url: '/admin/adminajax/additionallyfoto',
        type: $that.attr('method'),
        contentType: false, // важно - убираем форматирование данных по умолчанию
        processData: false, // важно - убираем преобразование строк по умолчанию
        data: formData,
        dataType: 'json',
        success: function(json){
          for (var i = 0; i < json.length ; i++) {
            var str = '<div><input hidden name="additiFoto[]" value="'+json[i]+'"><img class="img_dop" style="max-width:300px;" src="'+json[i]+'"><p class="del_img glyphicon glyphicon-remove"></p></div>';
            $('#main_block_add_img').append(str);
          }
          $('#imageMainAdd').parent().html($('#imageMainAdd').parent().html());
          delAddFoto();
          additionallyFoto();
          $('#addfotoid2').click(function(){
            $('#imageMainAdd').trigger('click');  
          })
        }
      });
     },1000);
    });
}

function delAddFoto(){
  var aaa;
  $('.del_img').click(function(e){
    var pathName = e.target.parentElement.children[0].value;
    delFoto(pathName);
    e.target.parentElement.remove();
    additionallyFoto();
  });
}

$( document ).ready(function() {
  delAddFoto();
  var form = $('.comment_obj_1').clone();
  form.appendTo('.form_a');

})

$('#addfotoid2').click(function(){
  $('#imageMainAdd').trigger('click');  
})
$('#addfotoid1').click(function(){
  $('#imageMain').trigger('click');  
})


/*----LIKES----*/

$(document).ready(function(){
  addlikeobject();
  dellikeobject();
})


function addlikeobject(){
  $('.button_like_add').click(function(){
    var count = $('.count_likes').html();
    count = +count+1;
    var block = $('#likes_object');
    block.html('<button class="button_like_del"><span class="glyphicon glyphicon-thumbs-up"></span> Likes <span class="count_likes">'+count+'</span></button>');
    dellikeobject();
    var id_object = $('#id_object').val();
     $.ajax({
        url: '/admin/adminajax/likeobject',
        type: 'post',
        data: 'id_object='+id_object,
        success: function(json){
          if (json) {
            return true;
          }
        }
      });
  })
}

function dellikeobject(){
  $('.button_like_del').click(function(){
    var count = $('.count_likes').html();
    count = +count-1;
    var block = $('#likes_object');
    block.html('<button class="button_like_add"><span class="glyphicon glyphicon-thumbs-up"></span> Likes <span class="count_likes">'+count+'</span></button>');
    addlikeobject();
    var id_object = $('#id_object').val();
     $.ajax({
        url: '/admin/adminajax/dellikeobject',
        type: 'post',
        data: 'id_object='+id_object,
        success: function(json){
          if (json) {
            return true;
          }
        }
      });
  })
}

/*----DISLIKES----*/
adddislikeobject();
deldislikeobject();

function adddislikeobject(){
  $('.button_dislike_add').click(function(){
    var count = $('.count_dislikes').html();
    count = +count+1;
    var block = $('#dislikes_object');
    block.html('<button class="button_dislike_del"><span class="glyphicon glyphicon-thumbs-down"></span> Dis Likes <span class="count_dislikes">'+count+'</span></button>');
    deldislikeobject();
    var id_object = $('#id_object').val();
     $.ajax({
        url: '/admin/adminajax/dislikeobject',
        type: 'post',
        data: 'id_object='+id_object,
        success: function(json){
          if (json) {
            return true;
          }
        }
      });
  })
}

function deldislikeobject(){
  $('.button_dislike_del').click(function(){
    var count = $('.count_dislikes').html();
    count = +count-1;
    var block = $('#dislikes_object');
    block.html('<button class="button_dislike_add"><span class="glyphicon glyphicon-thumbs-down"></span> Dis Likes <span class="count_dislikes">'+count+'</span></button>');
    adddislikeobject();
    var id_object = $('#id_object').val();
     $.ajax({
        url: '/admin/adminajax/deldislikeobject',
        type: 'post',
        data: 'id_object='+id_object,
        success: function(json){
          if (json) {
            return true;
          }
        }
      });
  })


}



$('.send_comment').click(function(e){

 var target = e.target;
 var parent  = $(this).parent().parent();
  setTimeout( function(){
     var el = $(".object_comment_class").find('.help-block-error');
    if(el[0].textContent.length == 0 && el[1].textContent.length == 0 && 
     el[2].textContent.length == 0){
     addComment(parent);
  }
  }, 1000);
  
});

function addComment(parent){
   
    var $that = parent;    
    formData = new FormData($that.get(0)); 
    $.ajax({
      url: '/ajax/addcomment',
      type: $that.attr('method'),
      contentType: false, // важно - убираем форматирование данных по умолчанию
      processData: false, // важно - убираем преобразование строк по умолчанию
      data: formData,
      dataType: 'json',
      success: function(json){

        if(json == true){
          $('#myModal').modal('show');   
          $('.author_name_obj').val(null);       
          $('.message_obj').val(null); 
          $('#good_msg').show(); 
          $('#error_msg').hide();          
          $('.answer_comment').hide();          
        }else{
          $('#myModal').modal('show'); 
          $('#error_msg').show();
          $('#good_msg').hide();
        }    

      }
    });
}





$(".comment_answer").click(function(){
  
     // var form = $(this).parent().children('.comment_obj');
     // console.log($(this).parent().children('.comment_obj'));
    // $(this).children().fadeIn(); 
   $(this).parent().children('.form_a').children().fadeIn();





 
});

$(".close_form").click(function(){
     var form = $(this).parent().parent();
     // console.log($(this).parent().parent());
    $(form).fadeOut();   
});




document.forms[0].onkeypress = function (a) {
    a = a || window.event;
    if (a.keyCode == 13 || a.which == 13)
    a.preventDefault ? a.preventDefault() : a.returnValue = false;
};

